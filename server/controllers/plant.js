const mongoose = require('mongoose');
const User = mongoose.model('User');
const Plant = mongoose.model('Plant');
const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: 'derryg8fc',
    api_key: '974446124972111',
    api_secret: 'waqwykofhvwGmulVbfo4je6jg2Q'
});

exports.add = function (req,res,next) {
    let obj = req.body;
    obj.user = req.user._id;
    if(req.files){
        let paths = [];
        for(let i = 1;i<=5;i++){
            if(req.files['avatar'+i]) paths.push(req.files['avatar'+i][0].path);
        }
        if(paths.length === 0){
            let plant = new Plant(obj);
            plant.save(function (err) {
                if(err) throw err;
                console.log('saved plant');
                res.redirect('/app/plants');
            });
        }else{
            exportImagesIntoCloudinary(cloudinary,paths,obj,function (obj) {
                let plant = new Plant(obj);
                plant.save(function (err) {
                    if(err) throw err;
                    console.log('saved plant');
                    res.redirect('/app/plants');
                });
            });
        }
    }else{
        let plant = new Plant(obj);
        plant.save(function (err) {
            if(err) throw err;
            console.log('saved plant');
            res.redirect('/app/plants');
        });
    }
};
function exportImagesIntoCloudinary(cloudinary,paths,obj,cb){
    cloudinary.uploader.upload(paths[0], function(result1) {
        obj.photoUrl1 = result1.url;
        if(paths[1]){
            cloudinary.uploader.upload(paths[1], function(result2) {
                obj.photoUrl2 = result2.url;
                if(paths[2]){
                    cloudinary.uploader.upload(paths[2], function(result3) {
                        obj.photoUrl3 = result3.url;
                        if(paths[3]){
                            cloudinary.uploader.upload(paths[3], function(result4) {
                                obj.photoUrl4 = result4.url;
                                if(paths[4]){
                                    cloudinary.uploader.upload(paths[4], function(result5) {
                                        obj.photoUrl5 = result5.url;
                                        cb(obj);
                                    });
                                }else{
                                    cb(obj);
                                }
                            });
                        }else{
                            cb(obj);
                        }
                    });
                }else{
                    cb(obj);
                }
            });
        }else{
            cb(obj);
        }
    });
}
exports.edit = function (req,res,next) {
    let obj = req.body.plant;
    Plant.findOne({_id: obj._id},function (err,plant) {
        plant.name = obj.name;
        plant.description = obj.description;
        plant.specie = obj.specie;
        plant.height = obj.height;
        plant.width = obj.width;
        plant.weight = obj.weight;
        plant.country = obj.country;
        plant.notes = obj.notes;
        plant.save(function (err,pl) {
            if(err) throw err;
            console.log('edited plant',pl.name);
            res.json();
        });
    });

};
exports.delete = function (req,res,next) {
    let plantId = req.body.id;
    Plant.remove({_id: plantId},function (err) {
        if(err) throw err;
        console.log('Deleted plant');
        res.json();
    });
};

exports.fetchYourPlants = function (req,res,next) {
    let userId = req.user._id;
    Plant.find({user: req.user._id},function (err, plants) {
        if(err) throw err;
        if(plants){
            res.json(plants);
        }else {
            console.log('there is no plants with id',userId);
            res.status(404).json({error: 'there is no plants' + userId});
        }
    })
};

exports.searchPlants = function (req,res,next) {
    let query = req.params.q;
    let regex = new RegExp('^'+query+'$');
    Plant.find({name: regex},function (err, plants) {
        if(err) throw err;
        if(plants){
            console.log('plants',plants);
            res.json(plants);
        }else {
            console.log('there is no plants with id',query);
            res.status(404).json({error: 'there is no plants' + query});
        }
    })
};

exports.reviewPlant = function (req, res, next) {
    let plantId = req.body.id;
    let review = req.body.review;
    Plant.findOne({_id: plantId},function (err, plant) {
        if(err) throw err;
        if(plant){
            let index = -1;
            plant.reviews.forEach(function (rev, ind) {
                if(rev.user == req.user._id){
                    index = ind;
                }
            });
            if(index > -1){
                plant.reviews[index].review = review;
            }else{
                let newObj = {
                    user: req.user._id,
                    username: req.user.name,
                    review
                };
                console.log('push into',newObj);
                plant.reviews.push(newObj);
            }
            plant.save(function (err,plant) {
                if(err) throw err;
                console.log('added new review',plant._id);
                res.json();
            })
        }else{
            console.log('error');
            res.status(404).json({error: 'there is no plants' + plantId});
        }
    })
};

exports.fetchPlant = function (req,res,next) {
    let id = req.params.id;
    Plant.findOne({_id: id},function (err, plant) {
        if(err) throw err;
        if(plant){
            res.json(plant);
        }else{
            console.log('there is no such a plant with',id);
            res.status(404).json({error: 'there is no such a plant with' + id});
        }
    })
};
