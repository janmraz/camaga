const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

var multer  = require('multer');
var upload = multer({ dest: './public/img/avatars/',
    inMemory: true });

// controllers
const PlantController = require('../controllers/plant');
// services
const passport = require('passport');
require('../services/passport.js'); // this needs to be run but is not directly referenced in this file

// session false as we are not using cookies, using tokens
const requireAuth = passport.authenticate('jwt', { session: false });

// ROUTES -----------------------------------------------------

// ADD PLANT
// add plant into DB
let cpUpload = upload.fields([
    { name: 'avatar1', maxCount: 1 },
    { name: 'avatar2', maxCount: 1 },
    { name: 'avatar3', maxCount: 1 },
    { name: 'avatar4', maxCount: 1 },
    { name: 'avatar5', maxCount: 1 }, ]);
router.post('/add',requireAuth, jsonParser, cpUpload, PlantController.add);

// EDIT PLANT
// edit plant
router.post('/edit',requireAuth, jsonParser, PlantController.edit);

// DELETE PLANT
// delete plant
router.post('/delete',requireAuth, jsonParser, PlantController.delete);

// REVIEW PLANT
// review plant
router.post('/review',requireAuth, jsonParser, PlantController.reviewPlant);

// FETCH ALL USER'S PLANTS
// fetch all user's plants from db
router.get('/own',requireAuth, jsonParser, PlantController.fetchYourPlants);

// SEARCH PLANTS
// fetch all plants by query from db
router.get('/search/:q',requireAuth, jsonParser, PlantController.searchPlants);

// FETCH PLANT
// fetch specific plant from db
router.get('/:id',requireAuth, jsonParser, PlantController.fetchPlant);


module.exports = router;
