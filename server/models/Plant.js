const mongoose = require('mongoose');

module.exports.init = function () {
    let plantSchema = new mongoose.Schema({
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now },
        photoUrl1: String,
        photoUrl2: String,
        photoUrl3: String,
        photoUrl4: String,
        photoUrl5: String,
        specie: String,
        seedBank: String,
        seedingType: String,
        growingType: String,
        growingPlace: String,
        growingTime: String,
        fenoType: String,
        crossingPlants: String,
        height: String,
        country: String,
        profit: String,
        ridigityMace: Number,
        amountRosin: Number,
        demandingness: Number,
        impact: String,
        tasteSmoke: String,
        smellSmoke: String,
        smell: String,
        totalReview: Number,
        fertilizer: String,
        notes: String,
        reviews: [{user: String, review: String, username: String}],
        user: String
    });

    let Plant = mongoose.model('Plant', plantSchema);
};
