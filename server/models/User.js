const mongoose = require('mongoose');

module.exports.init = function () {
    let userSchema = new mongoose.Schema({
        email:     { type: String, unique: true },
        name :     String,
        created: { type: Date, default: Date.now },
        password: String,
        emailConfirmed: false,
        emailConfirmCode: String,
        money: { type: Number, default: 50 }
    });

    let User = mongoose.model('User', userSchema);
};
