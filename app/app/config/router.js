// Router set up
const React = require('react');
const {Route, Router, IndexRoute, browserHistory} = require('react-router');

// Components
import Main from '../components/App/Main';
import Home from '../components/App/Home';
import About from '../components/App/About';
import Account from '../components/App/Account';
import InsertPlant from '../components/App/InsertPlant';
import UsersPlants from '../components/App/UsersPlants';
import Detail from '../components/App/Detail';
import Search from '../components/App/Search';
import Edit from '../components/App/Edit';
import EmailConfirm from '../components/App/EmailConfirm';

// Routes
const routes = (
    <Router history={browserHistory}> 
        <Route path="/app" component={Main}>
            <IndexRoute component={Home} />
            <Route path='/app/insert' header='Insert a plant' component={InsertPlant} />
            <Route path='/app/plants' header='Your plants' component={UsersPlants} />
            <Route path='/app/detail/:id' header='Detail about plant' component={Detail} />
            <Route path='/app/edit/:id' header='Detail about plant' component={Edit} />
            <Route path='/app/search/:q' header='Detail about plant' component={Search} />
            <Route path='/app/about' header='About' component={About} />
            <Route path='/app/account' header='User Account' component={Account} />
            <Route path='/app/email/:emailCode' header='Email Confirm' component={EmailConfirm} />
        </Route>
    </Router>
);

export default routes;