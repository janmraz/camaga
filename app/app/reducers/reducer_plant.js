import { ADD_PLANT, EDIT_PLANT, SEND_REVIEW, FETCH_PLANT, DELETE_PLANT, FETCH_YOUR_PLANTS, SEARCH_PLANTS } from '../actions/types.js';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
    let currState = { ...state };
    switch(action.type){
        case FETCH_PLANT:
            return { ...state, currentPlant: action.payload };
        case FETCH_YOUR_PLANTS:
            return { ...state, yourPlants: action.payload };
        case DELETE_PLANT:
             let arr = currState.yourPlants.filter(plant => plant._id !== action.payload);
             return {...state, yourPlants: arr };
        case SEARCH_PLANTS:
            return { ...state, searchPlants: action.payload };
        default:
            return state;
    }
}