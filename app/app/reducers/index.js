import { combineReducers } from 'redux';

// reducers
import UserReducer from './reducer_user';
import AlertReducer from './reducer_alert';
import PlantReducer from './reducer_plant';

const rootReducer = combineReducers({
  user: UserReducer,
  alerts: AlertReducer,
  plants: PlantReducer,
});

export default rootReducer;