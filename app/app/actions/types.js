// USER
export const FETCH_USER = 'FETCH_USER';
export const REMOVE_USER = 'REMOVE_USER';
export const EMAIL_CONFIRM = 'EMAIL_CONFIRM';

// ALERT
export const NEW_ALERT = 'NEW_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';

// PLANT
export const ADD_PLANT = 'ADD_PLANT';
export const EDIT_PLANT = 'EDIT_PLANT';
export const DELETE_PLANT = 'DELETE_PLANT';
export const SEARCH_PLANTS = 'SEARCH_PLANTS';
export const FETCH_YOUR_PLANTS = 'FETCH_YOUR_PLANTS';
export const FETCH_PLANT = 'FETCH_PLANT';
export const SEND_REVIEW = 'SEND_REVIEW';