import axios from 'axios';

// action types
import { ADD_PLANT, EDIT_PLANT, FETCH_PLANT, DELETE_PLANT, FETCH_YOUR_PLANTS, SEARCH_PLANTS, SEND_REVIEW } from './types';

// ----- THUNK ACTION CREATORS ------

// add plant
export function addPlant(obj) {
    return function(dispatch) {
        axios.post('/api/plant/add', {plant: obj})
            .then((response) => {
                dispatch({
                    type: ADD_PLANT,
                    payload: obj
                });
            })
            .catch(() => {
                throw new Error("New plant failed to save");
            });
    };
}
// send Review
export function sendReview(obj) {
    return function(dispatch) {
        axios.post('/api/plant/review', obj)
            .then((response) => {
                dispatch({
                    type: SEND_REVIEW,
                    payload: obj
                });
                location.reload();
            })
            .catch(() => {
                throw new Error("New plant failed to save");
            });
    };
}
// edit plant
export function editPlant(obj) {
    return function(dispatch) {
        axios.post('/api/plant/edit', {plant: obj})
            .then((response) => {
                dispatch({
                    type: EDIT_PLANT,
                    payload: obj
                });
            })
            .catch(() => {
                throw new Error("Editing plant failed to save");
            });
    };
}
// Fetch your plants into state
export function fetchYourPlants() {
    return function(dispatch) {
        axios.get('/api/plant/own', {
            headers: { authorization: "csrf token TO DO??" }
        })
            .then((response) => {
                dispatch({
                    type: FETCH_YOUR_PLANTS,
                    payload: response.data
                });
            })
            .catch((error) => {
                throw new Error("Your plants failed to fetch. Message from backend:" +error.error);
            });
    };
}
// Fetch plant into state
export function fetchPlant(id){
    return function (dispatch) {
        axios.get('/api/plant/'+id)
            .then((response) => {
                dispatch({
                    type: FETCH_PLANT,
                    payload: response.data
                })
            })
            .catch((error) => {
                throw new Error("Plant failed to fetch. Message from backend:" + error.error);
            });
    }
}
// Delete plant
export function deletePlant(id){
    return function (dispatch) {
        axios.post('/api/plant/delete',{id: id})
            .then((response) => {
                dispatch({
                    type: DELETE_PLANT,
                    payload: id
                })
            })
            .catch((error) => {
                throw new Error("Plant failed to delete. Message from backend:" + error.error);
            });
    }
}

// Search similar plants
export function searchPlants(q){
    return function (dispatch) {
        axios.get('/api/plant/search/'+ q)
            .then((response) => {
                dispatch({
                    type: SEARCH_PLANTS,
                    payload: response.data
                })
            })
            .catch((error) => {
                throw new Error("Failed to search. Message from backend:" + error.error);
            });
    }
}


