const React = require('react');
import { connect } from 'react-redux';
import { fetchPlant, sendReview } from '../../actions/actions_plant';

class Detail extends React.Component {
    constructor(){
        super();
        this.sendReview = this.sendReview.bind(this);
        this.onChange = this.onChange.bind(this);
        this.state = {
            review: ""
        }
    }
    componentDidMount(){
        this.props.dispatch(fetchPlant(this.props.routeParams.id));
    }
    sendReview(){
        let result = { review: this.state.review, id: this.props.plant._id };
        this.props.dispatch(sendReview(result));
    }
    onChange(event){
        this.setState({review: event.target.value});
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.plant) {
            let index = -1;
            nextProps.plant.reviews.forEach(function (rev, ind) {
                if (rev.user == nextProps.user.user_id) {
                    index = ind;
                }
            });
            if (index > -1) {
                this.setState({review: nextProps.plant.reviews[index].review});
            }
        }
    }
    render(){
        let _this = this;
        return (
            <div className="container">
                <h1>Detail</h1>
                <h3>Specie: {this.props.plant.specie}</h3>
                <div>Seed Bank: {this.props.plant.seedBank}</div>
                <div>Seeding Type: {this.props.plant.seedingType}</div>
                <div>Growing Type: {this.props.plant.growingType}</div>
                <div>Growing Place: {this.props.plant.growingPlace}</div>
                <div>Growing Time: {this.props.plant.growingTime}</div>
                <div>Fenotype: {this.props.plant.fenoType}</div>
                <div>Crossing Plants: {this.props.plant.crossingPlants}</div>
                <div>Height: {this.props.plant.height}</div>
                <div>Profit: {this.props.plant.profit}</div>
                <div>Country: {this.props.plant.country}</div>
                <div>Ridigity Mace: {this.props.plant.ridigityMace}</div>
                <div>Amount Rosin: {this.props.plant.amountRosin}</div>
                <div>Demandingness: {this.props.plant.demandingness}</div>
                <div>Impact: {this.props.plant.impact}</div>
                <div>Taste of Smoke: {this.props.plant.tasteSmoke}</div>
                <div>Smell of Smoke: {this.props.plant.smellSmoke}</div>
                <div>Smell: {this.props.plant.smell}</div>
                <div>Fertilizer: {this.props.plant.fertilizer}</div>
                <img src={this.props.plant.photoUrl1}/><br/>
                <img src={this.props.plant.photoUrl2}/><br/>
                <img src={this.props.plant.photoUrl3}/><br/>
                <img src={this.props.plant.photoUrl4}/><br/>
                <img src={this.props.plant.photoUrl5}/>
                <div>Total Review: {this.props.plant.totalReview}</div>
                <div>Notes: {this.props.plant.notes}</div>
                <div>
                    <h3>Reviews:</h3>
                    {(()=>{
                        if(_this.props.plant.reviews){
                            return <ul>
                                {_this.props.plant.reviews.map(function (review) {
                                    return <li key={review._id}>
                                        <h5>{review.username}</h5>
                                        <span>{review.review}</span>
                                    </li>
                                })}
                            </ul>
                        }
                    })()}
                </div>
                <div>
                    <h3>Add your review</h3>
                    <textarea value={_this.state.review} onChange={_this.onChange} />
                    <br/>
                    <button onClick={_this.sendReview}>Submit</button>
                </div>
            </div>
        );
    }
}
let mapStateToProps = function(state){
    return{ plant: state.plants.currentPlant || {},user: state.user }
};

export default connect(mapStateToProps)(Detail);
