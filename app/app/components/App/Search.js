const React = require('react');
import { connect } from 'react-redux';
import { searchPlants } from '../../actions/actions_plant';


import List from './shared/list';

class SearchPlants extends React.Component {
    constructor(){
        super();
    }
    componentDidMount(){
        this.props.dispatch(searchPlants(this.props.routeParams.q));
    }
    render(){
        let _this = this;
        return (
            <div className="container">
                <h1>Search for: {_this.props.routeParams.q}</h1>
                <List plants={_this.props.plants} deleteItem={_this.deleteItem} personal={false}/>
            </div>
        );
    }
}
let mapStateToProps = function(state){
    return{ plants: state.plants.searchPlants || [] }
};

export default connect(mapStateToProps)(SearchPlants);