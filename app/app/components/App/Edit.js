const React = require('react');
import { connect } from 'react-redux';
import { fetchPlant,editPlant } from '../../actions/actions_plant';
import { browserHistory } from 'react-router';

class Edit extends React.Component {
    constructor(){
        super();
        this.change = this.change.bind(this);
        this.submit = this.submit.bind(this);
        this.state = {
            specie: '',
            seedBank: '',
            seedingType: '',
            growingType: '',
            growingPlace: '',
            growingTime: '',
            fenoType: '',
            crossingPlants: '',
            height: '',
            country: '',
            profit: '',
            ridigityMace:1,
            amountRosin: 1,
            demandingness: 1,
            impact: '',
            tasteSmoke: '',
            smellSmoke: '',
            smell: '',
            totalReview: 1,
            fertilizer: '',
            notes: '',
        };
    }
    change(event,name){
        let currState = {...this.state};
        currState[name] = event.target.value;
        this.setState(currState);
    }
    submit(){
        this.props.dispatch(editPlant(this.state));
        browserHistory.push('/app/plants');
    }
    componentDidMount(){
        this.props.dispatch(fetchPlant(this.props.routeParams.id));
    }
    componentWillReceiveProps(nextProps) {
        this.setState(nextProps.plant);
    }
    render(){
        let _this = this;
        return (
            <div className="container">
                <h1>Edit</h1>
                    <label htmlFor="specie">Specie: </label>
                    <input type="text" name="specie" value={this.state.specie} onChange={(event) => {_this.change(event,'specie')}}/><br/><br/>
                    <label htmlFor="seedBank">Seed Bank: </label>
                    <input type="text" name="seedBank" value={this.state.seedBank} onChange={(event) => {_this.change(event,'seedBank')}}/><br/><br/>
                    <label htmlFor="fenoType">Feno Type: </label>
                    <select type="text" name="fenoType" value={this.state.fenoType} onChange={(event) => {_this.change(event,'fenoType')}}>
                        <option value="Indika">Indika</option>
                        <option value="Sativa">Sativa</option>
                        <option value="Ruderalis hybrid">Ruderalis hybrid</option>
                        <option value="Indika/Sativa">Indika/Sativa</option>
                        <option value="Sativa/Indika">Sativa/Indika</option>
                        <option value="Indika 50% / Sativa 50%">Indika 50% / Sativa 50%</option>
                    </select><br/><br/>
                    <label htmlFor="seedingType">Seeding Type: </label>
                    <select type="text" name="seedingType" value={this.state.seedingType} onChange={(event) => {_this.change(event,'seedingType')}}>
                        <option value="Clone">Clone</option>
                        <option value="Seed - regular">Seed - regular</option>
                        <option value="Seed - feminize">Seed - feminize</option>
                        <option value="Seed - automat">Seed - automat</option>
                    </select><br/><br/>
                    <label htmlFor="growingType">Growing Type: </label>
                    <select type="text" name="growingType"value={this.state.growingType} onChange={(event) => {_this.change(event,'growingType')}}>
                        <option value="Clay">Clay</option>
                        <option value="Hydro">Hydro</option>
                        <option value="Cocos">Cocos</option>
                    </select><br/><br/>
                    <label htmlFor="growingPlace">Growing Place: </label>
                    <select type="text" name="growingPlace"  value={this.state.specie} onChange={(event) => {_this.change(event,'growingPlace')}}>
                        <option value="Indoor">Indoor</option>
                        <option value="Outdoor">Outdoor</option>
                        <option value="Greenhouse">Greenhouse</option>
                    </select><br/><br/>
                    <label htmlFor="growingTime">Growing Time: </label>
                    <select type="text" name="growingTime" value={this.state.growingTime} onChange={(event) => {_this.change(event,'growingTime')}}>
                        <option value="18/6"> Indoor - 18/6</option>
                        <option value="12/12"> Indoor - 12/12</option>
                    </select><br/><br/>
                    <label htmlFor="crossingPlants">Crossing Plants: </label>
                    <input type="text" name="crossingPlants"  value={this.state.crossingPlants} onChange={(event) => {_this.change(event,'crossingPlants')}}/><br/><br/>
                    <label htmlFor="country">Country: </label>
                    <input type="text" name="country" value={this.state.country} onChange={(event) => {_this.change(event,'country')}}/><br/><br/>
                    <label htmlFor="ridigityMace">Ridigity Mace: </label>
                    <input type="number" name="ridigityMace" min="1" max="10" value={this.state.ridigityMace} onChange={(event) => {_this.change(event,'ridigityMace')}}/><br/><br/>
                    <label htmlFor="amountRosin">Amount Rosin: </label>
                    <input type="number" name="amountRosin" min="1" max="10" value={this.state.amountRosin} onChange={(event) => {_this.change(event,'amountRosin')}}/><br/><br/>
                    <label htmlFor="demandingness">Demandingness: </label>
                    <input type="number" name="demandingness" min="1" max="10" value={this.state.demandingness} onChange={(event) => {_this.change(event,'demandingness')}}/><br/><br/>
                    <label htmlFor="profit">Profit: </label>
                    <input type="text" name="profit" value={this.state.profit} onChange={(event) => {_this.change(event,'profit')}}/><br/><br/>
                    <label htmlFor="tasteSmoke">Taste of Smoke: </label>
                    <input type="text" name="tasteSmoke"  value={this.state.tasteSmoke} onChange={(event) => {_this.change(event,'tasteSmoke')}}/><br/><br/>
                    <label htmlFor="smellSmoke">Smell of Smoke: </label>
                    <input type="text" name="smellSmoke"  value={this.state.smellSmoke} onChange={(event) => {_this.change(event,'smellSmoke')}}/><br/><br/>
                    <label htmlFor="smell">Smell: </label>
                    <input type="text" name="smell" value={this.state.smell} onChange={(event) => {_this.change(event,'smell')}}/><br/><br/>
                    <label htmlFor="fertilizer">Fertilizer: </label>
                    <input type="text" name="fertilizer" value={this.state.fertilizer} onChange={(event) => {_this.change(event,'fertilizer')}}/><br/><br/>
                    <label htmlFor="height">Height: </label>
                    <input type="text" name="height" value={this.state.height} onChange={(event) => {_this.change(event,'height')}}/><br/><br/>
                    <label htmlFor="impact">Impact: </label>
                    <select type="text" name="impact" value={this.state.impact} onChange={(event) => {_this.change(event,'impact')}}>
                        <option value="Weak">Weak</option>
                        <option value="Medium">Medium</option>
                        <option value="Strong">Strong</option>
                    </select><br/><br/>
                    <label htmlFor="totalReview">Total Review: </label>
                    <input type="number" name="totalReview"  min="1" max="10" value={this.state.totalReview} onChange={(event) => {_this.change(event,'totalReview')}}/><br/><br/>
                    <textarea type="text" name="notes"  value={this.state.notes} onChange={(event) => {_this.change(event,'notes')}}/><br/><br/>
                    <input type="submit" value="Submit"/>
            </div>
        );
    }
}
let mapStateToProps = function(state){
    return{ plant: state.plants.currentPlant || {} }
};

export default connect(mapStateToProps)(Edit);