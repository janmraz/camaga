const React = require('react');
const {Link, IndexLink} = require('react-router');
import { browserHistory} from 'react-router'

class Nav extends React.Component {
    constructor(){
        super();
        this.onChange = this.onChange.bind(this);
        this.search = this.search.bind(this);
        this.state = {
          search: '',
        };
    }
    onChange(ref){
        this.setState({search: ref.target.value});
    }
    search(){
        browserHistory.push('/app/search/'+this.state.search)
    }

    render() {
        return (
            <nav className="navbar navbar-default navbar-static-top">
                <div className="col-md-10 col-md-offset-1">

                    <div className="navbar-header">
                      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-collapse" aria-expanded="false">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                      </button>
                      <IndexLink to="/app" className="navbar-brand" activeClassName="active">Ca-Ma-Ga</IndexLink>
                    </div>
                    
                    <div className="collapse navbar-collapse" id="menu-collapse">
                    
                        <ul className="nav navbar-nav">
                            <li>
                                <Link to="/app/about" activeClassName="active">About</Link>
                            </li>
                        </ul>
                        <div className="col-sm-3 col-md-3 navbar-right">
                            <form className="navbar-form" onSubmit={this.search} role="search">
                                <div className="input-group">
                                    <input type="text" className="form-control" value={this.state.search} onChange={this.onChange} placeholder="Search" name="q" />
                                        <div className="input-group-btn">
                                            <button className="btn btn-default" onClick={this.search} type="submit"><i className="glyphicon glyphicon-search"></i></button>
                                        </div>
                                </div>
                            </form>
                        </div>
                        <ul className="nav navbar-nav navbar-right">
                            <li>
                                <Link to="/app/insert" activeClassName="active">Insert plant</Link>
                            </li>
                            <li>
                                <Link to="/app/plants" activeClassName="active">Your plants</Link>
                            </li>
                            <li>
                                <Link to="/app/account" activeClassName="active">Account</Link>
                            </li>
                        </ul>
                    
                    </div>
                
                </div>
            </nav>
        );
    }
}

export default Nav;