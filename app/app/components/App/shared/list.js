const React = require('react');
import { Link } from 'react-router';

class list extends React.Component {
    constructor(){
        super();
    }
    render(){
        let _this = this;
        return <ul>
            {this.props.plants.map(function(plant){
                let detailUrl = '/app/detail/'+ plant._id;
                let editUrl = '/app/edit/'+ plant._id;
                return <li key={plant._id}>
                    <span>{plant.specie}</span>
                    &nbsp;&nbsp;&nbsp;
                    <Link to={detailUrl}>Detail</Link>
                    &nbsp;&nbsp;&nbsp;
                    {(() => {
                        if(_this.props.personal) {
                            return <Link to={editUrl}>Edit</Link>
                        }
                    })()}
                    &nbsp;&nbsp;&nbsp;
                    {(() => {
                        if(_this.props.personal) {
                            return <a onClick={(() => {_this.props.deleteItem(plant._id)})}>Delete</a>
                        }
                    })()}
                </li>;
            })}
        </ul>
    }
}

export default list;