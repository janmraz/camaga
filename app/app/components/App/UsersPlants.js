const React = require('react');
import { connect } from 'react-redux';
import { fetchYourPlants,deletePlant } from '../../actions/actions_plant';


import List from './shared/list';

class UsersPlants extends React.Component {
    constructor(){
        super();
        this.deleteItem = this.deleteItem.bind(this);
    }
    componentDidMount(){
        this.props.fetchYourPlants();
    }
    deleteItem(id){
        this.props.deletePlant(id);
    }
    render(){
        let _this = this;
        return (
            <div className="container">
                <h1>There are your plants</h1>
                <List plants={_this.props.plants} deleteItem={_this.deleteItem} personal={true}/>
            </div>
        );
    }
}
let mapStateToProps = function(state){
    return{ plants: state.plants.yourPlants || [] }
};

export default connect(mapStateToProps,{ fetchYourPlants, deletePlant })(UsersPlants);